#!/usr/bin/env python
import socket
import os
import pytz
from umodbus import conf
from umodbus.client import tcp
from datetime import datetime
import sundial
from sundial.rest import ApiException
from sundial.configuration import Configuration

SLAVE_ID = 1

api_key = os.environ.get('SUNDIAL_API_KEY')
port = int(os.environ.get('SUNDIAL_MODBUS_PORT'))
plant_id = os.environ.get('SUNDIAL_PLANT_ID')

measurements = [
    {
        'name': 'plant-a-power',
        'address': 100
    },
    {
        'name': 'plant-b-power',
        'address': 101
    }
]

configuration = Configuration()
configuration.host = os.environ.get('SUNDIAL_URL')

# create an instance of the API class
api_instance = sundial.AdviceControllerApi(
    sundial.ApiClient(configuration, 'Authorization', f'Bearer {api_key}'))


# TODO: ensure against comms failure
def get_plant_recommendations():
    api_response = api_instance.advice_controller_get_plant_advice(
        plant_id)
    return api_response.recommendations


def main():
    # Enable values to be signed (default is False).
    conf.SIGNED_VALUES = True

    port = int(os.environ.get('SUNDIAL_MODBUS_PORT'))

    while True:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('localhost', port))

        now = datetime.utcnow().replace(tzinfo=pytz.utc)
        recommendations = get_plant_recommendations()

        # Find the current recommendation
        current_recommendations_iterator = filter(
            lambda r: r.start_at <= now and r.end_at >= now, recommendations)

        current_recommendations = list(current_recommendations_iterator)

        if not len(current_recommendations):
            print('No current recommendation')
            continue

        recommendation = current_recommendations[0]

        # Set point based on current recommendation
        if recommendation.supply_to_grid:
            set_point = 5000
        else:
            set_point = 0

        message = tcp.write_single_register(
            slave_id=SLAVE_ID, address=12, value=set_point)
        response = tcp.send_message(message, sock)
        print(f'Written set point: {response}')

        # Read some measurements
        for measurement in measurements:
            message = tcp.read_input_registers(
                slave_id=SLAVE_ID, starting_address=measurement['address'], quantity=1)
            response = tcp.send_message(message, sock)
            print(f'Measurement: {response}')
            # TODO: send measurement to Sundial

        sock.close()


if __name__ == '__main__':
    main()
