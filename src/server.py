#!/usr/bin/env python
# scripts/examples/simple_tcp_server.py
from __future__ import print_function
import logging
from socketserver import TCPServer, ThreadingMixIn
from collections import defaultdict
from cachetools import cached, TTLCache
import dateutil.parser
from threading import Lock
import time
import os
import sundial
import pytz
from pprint import pprint

from umodbus import conf
from umodbus.server.tcp import RequestHandler, get_server
from umodbus.utils import log_to_stream

utc = pytz.UTC

SLAVE_ID = 1

# Add stream handler to logger 'uModbus'.
log_to_stream(level=logging.DEBUG)

# Enable values to be signed (default is False).
conf.SIGNED_VALUES = True
port = int(os.environ.get('SUNDIAL_MODBUS_PORT'))
set_point = 5000


class ThreadingServer(ThreadingMixIn, TCPServer):
    pass


ThreadingServer.allow_reuse_address = True
app = get_server(ThreadingServer, ('', port), RequestHandler)
lock = Lock()


def toSigned(n, byte_count):
    return int.from_bytes(n.to_bytes(byte_count, 'little'), 'little', signed=True)


@app.route(slave_ids=[SLAVE_ID], function_codes=[4], addresses=[100, 101])
def read_measurement(slave_id, function_code, address):
    """Return measurement"""
    global set_point
    return set_point


@app.route(slave_ids=[SLAVE_ID], function_codes=[6], addresses=[12])
def configure_set_point(slave_id, function_code, address, value):
    """Configure set point"""
    global set_point
    set_point = value


def main():
    try:
        app.serve_forever()
    finally:
        app.shutdown()
        app.server_close()


if __name__ == '__main__':
    main()
