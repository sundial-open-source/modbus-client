# Sundial Modbus Client

This client reads advice from Sundial, and gives it to
a local device.

It reads measurements from a local device, and gives those
to Sundial.

## Mappings

The client reads advice for a single plant.

It reads whatever measurements you want from the plant, and
reports them back to Sundial.

## Requirements

You need Python 3.x and `pipenv`

## Preparing

- Navigate to the source folder
- Run `pipenv shell`
- Run `pipenv install`

## Running

### Client

```
SUNDIAL_API_KEY=<my sundial API key> SUNDIAL_URL=https://api.sundial.energy SUNDIAL_PLANT_ID=5 SUNDIAL_MODBUS_PORT=<some port> python ./client.py
```

### Server

This repository comes with a test server.

It simulates a plant, with two halves (based on the real-life Chillamurra example).

It has a register that controls power production at the plant (set point).

It has two registers which report power generation from the two halves of the plant.

Reported power is constant at this stage, although it respects the "power production set point" register.

To run the server:

```
SUNDIAL_MODBUS_PORT=<some port> python server.py
```
